var editors = {};
let schemas = {};

var initAndLoadSchemaEditor = function(context) {
    let formId = context.id;
    let editorId = 'editor-' + context.id;
    let schemaSelectorFieldName = context.schema_selector_field_name;
    let default_values = context.default_values;
    schemas = context.schemas;

    let selectorId = formId.replace(
        'customization_args', schemaSelectorFieldName);
    let schemaSelector = document.getElementById(selectorId);

    schemaSelector.addEventListener('change', (event) => {
        let startval = default_values[event.target.value];
        loadSchemaEditor(editorId, formId, schemaSelector.value, startval);
    });

    let selectorValue = schemaSelector.value;

    let startval = JSON.parse(context.startval);
    if (!startval) {
        startval = default_values[selectorValue];
    }

    loadSchemaEditor(editorId, formId, selectorValue, startval);
};

function loadSchemaEditor(editorId, formId, interface_type, startval){
    if (editors.hasOwnProperty(editorId)) {
        editors[editorId].destroy();
    }

    if (!interface_type) {
        document.getElementById(formId).innerHTML = null;
        return;
    }

    let options = {
        schema: schemas[interface_type],
        disable_edit_json: true,
        disable_collapse: true,
        startval: startval,
        theme: 'bootstrap4',
    }

    let editor = new JSONEditor(
        document.getElementById(editorId), options);

    editors[editorId] = editor;
    editor.on('change', function(e){
        document.getElementById(formId).innerHTML = JSON.stringify(editor.getValue());
    });
};
