"""
Tests for ApiLocaleMiddleware
"""
from unittest.mock import Mock

from django.conf import settings

from middlewares.api_locale import ApiLocaleMiddleware


def mock_get_response(request):
    """
    Mock for get_response method
    """
    return request


def test_detect_request_locale():
    """
    Test detect request's locale based on its header
    """
    tested_languages = ["en", "zh", "vi", "ar", "hr", "es", "bg"]
    middleware = ApiLocaleMiddleware(mock_get_response)
    request = Mock()
    request.path = "/"
    request.headers = {}
    for lang in tested_languages:
        request.headers[settings.API_LANGUAGE_HEADER] = lang
        processed_request = middleware.process_request(request)
        assert processed_request.LANGUAGE_CODE == lang
