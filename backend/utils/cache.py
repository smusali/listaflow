"""
Cache related utilities
"""

from django.conf import settings
from django.core.cache import cache as default_cache_client

CACHE_KEY_DELIMITER = settings.CACHE_KEY_DELIMITER


def make_cache_key(*args) -> str:
    """
    Make cache key using array of arguments

    Args:
        args: list of items used for generating key
    """
    keys = []
    for item in args:
        if not isinstance(item, str) and item is not None:
            item = str(item)
        if item:
            keys.append(item)
    return CACHE_KEY_DELIMITER.join(keys)


def make_dict_cache_key(obj):
    """
    Generate cache key for an dict based on id

    Args:
        obj: dict. An dict of data
    """
    pk_field = obj.get("id")
    if not pk_field:
        raise ValueError("Please include `id` field in dict for cache key")
    return pk_field


def make_pk_cache_key(instance):
    """
    Generate cache key for an instance based on its meta

    Args:
        instance: Model. An instance of Django model
    """
    model_name = instance.__class__.__name__.lower()
    pk_field = instance._meta.pk.name
    pk_value = instance.pk
    pk_cache_key = make_cache_key(model_name, pk_field, pk_value)
    return pk_cache_key


def set_cache(key: str, value: any, cache_cli=default_cache_client):
    """
    Set cache using a cache_cli

    Args:
        key: str. Cache key
        value: any. Cache value
        cache_cli: any. Cache client to use
    """
    cache_cli.set(key, value)


def get_cache(key: str, cache_cli=default_cache_client):
    """
    Get cache for a given cache key

    Args:
        key: str. Cache key
        cache_cli: any. Cache client to use
    """
    if key is None:
        return None
    return cache_cli.get(key)


def clear_cache(key: str, cache_cli=default_cache_client):
    """
    Clear cache for a given key

    Args:
        key: str. Cache key
        cache_cli: any. Cache client to use
    """
    cache_cli.delete(key=key)


def clear_cache_multi(key_regex: str, cache_cli=default_cache_client):
    """
    Clear all cache for a given key regex
    Works for redis cache only

    Args:
        key_regex: str. Cache key
        cache_cli: any. Cache client to use
    """
    cache_cli.delete_many(keys=cache_cli.keys(key_regex))
