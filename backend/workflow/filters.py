"""Workflow filters"""
from django_filters import rest_framework as filters
from django.db import models
from workflow.enums import ChecklistStatus
from workflow.models import Checklist


class ChecklistFilter(filters.FilterSet):
    """Filter for workflow checklists"""

    usernames = filters.CharFilter(method="filter_usernames")
    statuses = filters.CharFilter(method="filter_statuses")
    is_archived = filters.BooleanFilter(field_name="is_archived")

    class Meta:
        model = Checklist
        fields = ["usernames", "statuses", "is_archived", "name"]
        filter_overrides = {
            models.CharField: {
                "filter_class": filters.CharFilter,
                "extra": lambda f: {
                    "lookup_expr": "icontains",
                },
            }
        }

    def filter_statuses(self, queryset, _, value):  # pylint: disable=no-self-use
        """Filter by checklist statuses"""
        user = self.request.user
        statuses = value.split(",")
        valid_statuses: list = []
        for status in statuses:
            if status in ChecklistStatus.names():
                valid_statuses.append(status)
        return queryset.filter_by_statuses(valid_statuses, user)

    def filter_usernames(self, queryset, _, value):  # pylint: disable=no-self-use
        """Filter by assignees usernames"""
        usernames: list[str] = value.split(",")
        return queryset.filter_by_assignees(usernames)
