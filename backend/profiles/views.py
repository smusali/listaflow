"""
Views for anything related to user profile
"""

# Create your views here.
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from oauth2_provider.models import AccessToken, Application, RefreshToken
from oauth2_provider.settings import oauth2_settings
from oauthlib import common
from rest_framework import mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_registration import signals
from rest_registration.api.views.register import process_verify_registration_data
from rest_registration.decorators import api_view_serializer_class
from rest_registration.settings import registration_settings
from rest_registration.utils.responses import get_ok_response
from rest_registration.utils.users import get_user_by_verification_id
from rest_registration.utils.verification_notifications import (
    send_register_verification_email_notification,
)
from lib.permissions import IsSafeMethod


from profiles.models import Team
from profiles.serializers import (
    CustomVerifyRegistrationSerializer,
    ResendEmailVerificationSerializer,
    TeamSerializer,
    TeamNameSerializer,
)


@api_view(["POST"])
@permission_classes([AllowAny])
def resend_verification_email(request):
    """
    Resend verification email
    """
    serializer = ResendEmailVerificationSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        user = get_user_by_verification_id(
            serializer.validated_data["id"], require_verified=False
        )
        if not user.is_active:
            send_register_verification_email_notification(request, user)
    return Response(status=200)


@api_view_serializer_class(CustomVerifyRegistrationSerializer)
@api_view(["POST"])
@permission_classes([AllowAny])
def verify_registration(request):
    """
    Verify registration via signature.
    """
    user = process_verify_registration_data(
        request.data, serializer_context={"request": request}
    )
    signals.user_activated.send(sender=None, user=user, request=request)
    extra_data = None
    if registration_settings.REGISTER_VERIFICATION_AUTO_LOGIN:
        serializer = CustomVerifyRegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        application = Application.objects.filter(
            client_id=serializer.data["client_id"]
        ).first()
        expires = timezone.now() + timezone.timedelta(
            seconds=oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS
        )
        access_token = AccessToken(
            user=user,
            scope="read write groups",
            expires=expires,
            token=common.generate_token(),
            application=application,
        )
        access_token.save()
        refresh_token = RefreshToken(
            user=user,
            token=common.generate_token(),
            application=application,
            access_token=access_token,
        )
        refresh_token.save()
        extra_data = {
            "access_token": access_token.token,
            "expires_in": oauth2_settings.ACCESS_TOKEN_EXPIRE_SECONDS,
            "token_type": "Bearer",
            "scope": access_token.scope,
            "refresh_token": refresh_token.token,
        }
    return get_ok_response(_("User verified successfully"), extra_data=extra_data)


class UserTeamViewSet(mixins.ListModelMixin, GenericViewSet):
    """User team listing view"""

    permission_classes = [IsAuthenticated, IsSafeMethod]
    serializer_class = TeamSerializer
    pagination_class = None

    def get_queryset(self):
        """
        Override to return teams of user only
        """
        if getattr(self, "swagger_fake_view", False):
            return Team.objects.all()
        user = self.request.user
        return user.teams.all()


class UserTeamNamesViewSet(mixins.ListModelMixin, GenericViewSet):
    """User team listing view"""

    permission_classes = [IsAuthenticated]
    serializer_class = TeamNameSerializer
    pagination_class = None

    def get_queryset(self):
        """
        Override to return teams of user only
        """
        user = self.request.user
        if user.is_superuser:
            return Team.objects.all()
        return user.teams.all()
