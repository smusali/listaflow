"""
social pipeline functions
"""

from social_core.pipeline.user import create_user as social_create_user


def create_user(strategy, details, backend, *args, user=None, **kwargs):
    """
    set first name as display name
    """
    details["display_name"] = details.get("first_name")
    return social_create_user(
        strategy=strategy, details=details, backend=backend, user=user, *args, **kwargs
    )
