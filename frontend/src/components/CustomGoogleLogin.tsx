import useAuth from "../hooks/useAuth";
import { useNavigate, useLocation } from "react-router-dom";
import { LocationState } from "../types/Common";
import GoogleLogin, {
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from "react-google-login";
import axios from "axios";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { ReactComponent as GoogleSvg } from "../assets/icons/google.svg";
import { Button } from "../components/Button";
import { useState } from "react";
import { Divider } from "./Divider";
import { googleRedirectUri } from "../utils/helpers";
import { useTranslation } from "react-i18next";
import { HOME_PAGE } from "../constants/urls";

declare interface CustomGoogleLoginArgs {
  setErrors: (errors: string[]) => void;
}

export const CustomGoogleLogin = ({ setErrors }: CustomGoogleLoginArgs) => {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  const { googleLogin, logoutUser } = useAuth();
  const navigate = useNavigate();
  const location = useLocation();
  const locationState = location.state as LocationState;
  if (!process.env.REACT_APP_GOOGLE_CLIENT_ID) {
    return <></>;
  }

  const responseGoogle = async (
    response: GoogleLoginResponse | GoogleLoginResponseOffline
  ) => {
    setIsLoading(true);
    try {
      if ("accessToken" in response) {
        await googleLogin(response.accessToken);
        navigate(locationState?.from?.pathname || HOME_PAGE, { replace: true });
      }
      await logoutUser();
    } catch (error) {
      await logoutUser();
      if (axios.isAxiosError(error)) {
        setErrors([error.response?.data?.detail]);
      } else {
        throw error;
      }
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Row className="d-flex justify-content-center my-5">
        <Col>
          <GoogleLogin
            isSignedIn
            uxMode="redirect"
            redirectUri={googleRedirectUri()}
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID!}
            onRequest={() => setIsLoading(true)}
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={"single_host_origin"}
            render={(renderProps) => (
              <Button
                variant="outline"
                className="w-100 border-1 border-info rounded-3"
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
                loading={isLoading}
              >
                <GoogleSvg className="me-2 mb-1" style={{ height: "20px" }} />
                {t("loginWithGoogle")}
              </Button>
            )}
          />
        </Col>
      </Row>
      <Divider text="OR" />
    </>
  );
};
