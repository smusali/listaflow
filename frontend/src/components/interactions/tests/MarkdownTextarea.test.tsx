import { useSingle } from "@opencraft/providence/react-plugin";

import { fireEvent, render, screen, waitFor } from "../../../utils/test-utils";
import { MarkdownTextarea } from "../MarkdownTextarea";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { useTranslation } from "react-i18next";

let testController: SingleController<InterfaceComponentProps>;
const { t } = useTranslation();

jest.mock("react-markdown", () => {
  return () => {
    return <p>Markdown preview</p>;
  };
});

const TestComponent = () => {
  testController = useSingle<InterfaceComponentProps>("testController", {
    endpoint: "#",
    x: {
      customizationArgs: { placeholder: "Type your text here..." },
      response: null,
    },
  });

  return (
    <MarkdownTextarea
      customizationArgs={testController.x!.customizationArgs}
      response={testController.p.response}
    />
  );
};

describe("MarkdownTextarea component", () => {
  beforeEach(async () => {
    render(<TestComponent />);
  });

  it("should render textarea correctly", async () => {
    const textarea = screen.getByPlaceholderText("Type your text here...");
    expect(textarea).toBeInTheDocument();
  });

  it("should update response correctly on updating the textarea value", async () => {
    const textarea = screen.getByPlaceholderText("Type your text here...");
    fireEvent.change(textarea, {
      target: { value: "Some text" },
    });

    expect(testController.p.response.model).toStrictEqual({
      content: "Some text",
    });
  });

  it("should correctly highlight active tab", async () => {
    const writeTabButton = screen.getByText(t("interactions.write"));
    const previewTabButton = screen.getByText(t("interactions.preview"));

    expect(writeTabButton.className).toContain("markdown-editor-active-tab");
    expect(previewTabButton.className).not.toContain(
      "markdown-editor-active-tab"
    );

    fireEvent.click(previewTabButton);
    expect(writeTabButton.className).not.toContain(
      "markdown-editor-active-tab"
    );
    expect(previewTabButton.className).toContain("markdown-editor-active-tab");
  });

  it("should remove textarea when preview tab is clicked", async () => {
    const writeTabButton = screen.getByText(t("interactions.write"));
    const previewTabButton = screen.getByText(t("interactions.preview"));

    fireEvent.click(writeTabButton);
    let textarea = screen.getByPlaceholderText("Type your text here...");
    expect(textarea).toBeInTheDocument();

    fireEvent.click(previewTabButton);
    expect(textarea).not.toBeInTheDocument();
  });

  it("should remove markdown when write tab is clicked", async () => {
    const writeTabButton = screen.getByText(t("interactions.write"));
    const previewTabButton = screen.getByText("interactions.preview");

    fireEvent.click(previewTabButton);
    const markdownPreview = screen.getByText("Markdown preview");
    expect(markdownPreview).toBeInTheDocument();

    fireEvent.click(writeTabButton);
    expect(markdownPreview).not.toBeInTheDocument();
  });
});
