import { useState, useEffect } from "react";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import Accordion from "react-bootstrap/Accordion";
import { Task } from "../types/Task";
import { Body } from "./Body";
import { Title } from "./Title";
import { Interaction } from "./interactions/Interaction";
import { CheckboxHeader } from "./CheckboxHeader";
import { useTranslation } from "react-i18next";

declare interface TaskItemBase {
  controller: SingleController<Task>;
}

export const TaskItem = ({ controller }: TaskItemBase) => {
  let [warningMessageVisible, setWarningMessageVisible] = useState(false);
  const { t } = useTranslation();

  const body = controller.x?.body || "";
  const showExpansion = body || controller.x?.interface_type !== "checkbox";

  useEffect(() => {
    setWarningMessageVisible(false);
  }, [controller.p.response.model]);

  return (
    <Accordion.Item
      className="border-top border-light-navy border-2"
      eventKey={controller.x!.id}
      key={controller.x!.id}
    >
      <Accordion.Button
        tabIndex={showExpansion ? undefined : -1}
        aria-label={showExpansion ? t("taskItem.showDetails") : ""}
        className={showExpansion ? "" : "hide-toggle-button"}
      >
        {controller.x!.required && (
          <span className="checklist-required-marker">*</span>
        )}
        <div className="w-100 d-flex justify-content-between">
          {controller.x?.interface_type === "checkbox" ? (
            <CheckboxHeader
              response={controller.p.response}
              label={controller.x!.label}
              id={controller.x!.id}
            />
          ) : (
            <div dir="auto">
              <Title text={controller.x!.label} />
            </div>
          )}
        </div>
      </Accordion.Button>
      {showExpansion && (
        <Accordion.Body
          className={`py-2 ${
            controller.x?.interface_type === "checkbox" ? "ms-45" : "mb-3"
          }`}
        >
          {body && <Body text={controller.x!.body} />}
          {controller.x?.interface_type !== "checkbox" && (
            <div
              className={
                warningMessageVisible ? "incomplete-warning-border" : ""
              }
            >
              <Interaction
                interfaceType={controller.x!.interface_type}
                customizationArgs={controller.x?.customization_args}
                response={controller.p.response}
                required={controller.x?.required}
              />
            </div>
          )}
          {warningMessageVisible && (
            <p className="text-danger mt-1">
              {t("taskItem.respondBeforeComplete")}
            </p>
          )}
        </Accordion.Body>
      )}
    </Accordion.Item>
  );
};
