import { act, fireEvent, render, screen } from "../../../utils/test-utils";
import AnswerTable from "../AnswerTable";
import { CompareReportTable, UserTaskResponseMap } from "../../../types/Report";

const TestComponentMarkdown = () => {
  const tasks: UserTaskResponseMap = {
    user1: { assignee_name: "User 1", date1: "value1", date2: "value2" },
  };
  const data: CompareReportTable = {
    completed: 1,
    total: 2,
    dates: ["date1", "date2", "date3", "date4"],
    tasks: tasks,
    interface_type: "markdown_textarea",
    task_label: "task_label",
  };

  return <AnswerTable data={data} />;
};

describe("AnswerTable with markdown content", () => {
  beforeEach(() => {
    render(<TestComponentMarkdown />);
  });

  it("should render markdown table correctly", async () => {
    const table = screen.getByRole("table");
    expect(table).toBeInTheDocument();
  });

  it("should render markdown cells", async () => {
    const cell = screen.getByText("value1");
    expect(cell.className).toContain("markdown-table-cell");
  });

  it("should render next button in header", async () => {
    const button = screen.getByRole("button");
    expect(button).toBeInTheDocument();
    expect(button.textContent).toBe(">");
  });

  it("should render prev button in header", async () => {
    const button = screen.getByRole("button");
    act(() => {
      fireEvent.click(button);
    });
    const prevButton = screen.getByText("<");
    expect(prevButton).toBeInTheDocument();
  });
});
